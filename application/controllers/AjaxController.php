<?php

class AjaxController extends Zend_Controller_Action
{
    public function init(){
        parent::init();

        $this->_helper->viewRenderer->setNoRender();
        $this->_helper->layout()->disableLayout();
    }

    public function getMetroAction()
    {
        $city = $this->getParam('city');
        if(empty($city) && !in_array($city, ['m', 'sp'])) echo '';

        $m = [
            'ms1' => 'm-station-1',
            'ms2' => 'm-station-2',
            'ms3' => 'm-station-3',
            'ms4' => 'm-station-4',
        ];
        $sp = [
            'sps1' => 'sp-station-1',
            'sps2' => 'sp-station-2',
            'sps3' => 'sp-station-3',
            'sps4' => 'sp-station-4',
        ];

        echo Zend_Json::encode($$city);
    }
}