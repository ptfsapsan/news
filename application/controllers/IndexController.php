<?php

class IndexController extends Zend_Controller_Action
{

    public function postDispatch()
    {
        parent::postDispatch();

        $request = $this->_request;
        $writer = new Zend_Log_Writer_Stream(APPLICATION_PATH . '/data/logs/logs.log');
        $logger = new Zend_Log($writer);

        $format = 'Время начала запроса - %timestamp%' . PHP_EOL . '%message%' . PHP_EOL;
        $formatter = new Zend_Log_Formatter_Simple($format);
        $writer->setFormatter($formatter);
        $info = 'Продолжительность выполнения скрипта во время действия пользователя - ' .
            (microtime(true) - $request->getServer('REQUEST_TIME_FLOAT')) . ' секунд' . PHP_EOL .
            'IP-адрес пользователя - ' . $request->getServer('SERVER_ADDR') . PHP_EOL .
            'URL, по которому выполнялось действие - ' .
                $this->view->serverUrl() . $request->getServer('REQUEST_URI') . PHP_EOL;
        $post = $request->getPost();
        if(count($post))
            $info .= '$_POST - ' . Zend_Json::encode($post) . PHP_EOL;

        $logger->log($info, Zend_Log::INFO);
    }

    public function indexAction()
   {
       $view = $this->view;
       $fm = $this->_helper->getHelper('FlashMessenger');
        $form_user = new Form_User();
        $form_company = new Form_Company();
        $params = $this->getAllParams();
        $model_users = new Model_Users();
        if(!empty($params['id'])){
            $user = $model_users->getById($params['id']);
            if(empty($user)) $this->redirect($view->url());

            $form_user->act->setValue('edit_user');
            $form_user->user_id->setValue($user['id']);
            $form_user->populate($user);
        }

        if(!empty($params['act'])){
            switch ($params['act']){
                case 'add_user':
                    if($form_user->isValid($params)) {
                        $model_users->addUser($params);
                        $fm->addMessage('Пользователь добавлен', 'info');
                        $this->redirect('/list');
                    } else {
                        $fm->addMessage($form_user->getMessages(), 'error');
                    }
                    break;
                case 'edit_user':
                    if($form_user->isValid($params)) {
                        $model_users->editUser($params);
                        $fm->addMessage('Данные пользователя изменены', 'info');
                        $this->redirect('/list');
                    } else {
                        $fm->addMessage($form_user->getMessages(), 'error');
                    }
                    break;
                case 'add_company':

                    break;
            }
            $this->redirect($view->url());
        }


        $view->assign([
            'form_user' => $form_user,
            'form_company' => $form_company,
        ]);
   }

   public function listAction()
   {
       $model_users = new Model_Users();


       $this->view->assign([
           'users' => $model_users->getAll(),
       ]);
   }



} 