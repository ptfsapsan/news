<?php
class ErrorController extends Zend_Controller_Action {

	public function errorAction(){
		
		$error = $this->_getParam('error_handler');
        if (!$error) {
            $this->view->message = 'You have reached the error page';
            return;
        }
		switch($error->type){
            case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ROUTE:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_CONTROLLER:
			case Zend_Controller_Plugin_ErrorHandler::EXCEPTION_NO_ACTION:
				$this->_forward('error404');
				break;
			default:
				$this->getResponse()->setHttpResponseCode(500);
				$this->view->message = 'Application error';
		}
		
		// log
        $bootstrap = $this->getInvokeArg('bootstrap');
		if($bootstrap->hasResource('Log')){
			$log = $bootstrap->getResource('Log');
			$log->crit($this->view->message, $error->exception);
		}
        
        if ($this->getInvokeArg('displayExceptions') == 1)
            $this->view->exception = $error->exception;
		$this->view->env = APPLICATION_ENV;
		$this->view->request = $error->request;
    }

	public function error404Action(){
        $this->getResponse()->setHttpResponseCode(404);
        $this->view->message = 'Page not found';	}
}

