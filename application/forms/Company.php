<?php

class Form_Company extends Zend_Form
{
    public function init()
    {
        $this
            ->setDecorators([
                'FormElements',
                ['HtmlTag', ['tag' => 'table']],
                'Form'
            ])
            ->addElement('hidden', 'act', [
                'value' => 'add_company',
            ])
            ->addElement('text', 'title', [
                'label' => 'Название компании',
                'required' => true,
            ])
            ->addElement('text', 'legal_address', [
                'label' => 'Юридический адрес',
            ])
            ->addElement('text', 'checking_account', [
                'label' => 'Расчётный счёт',
            ])
            ->addElement('submit', 'submit', [
                'label' => 'Отправить',
            ])
        ;

        foreach ($this as $element) {
            $element->setDecorators([
                new Form_Decorator_Decorator(),
            ]);
        }
    }
}