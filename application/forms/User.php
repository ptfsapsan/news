<?php

class Form_User extends Zend_Form
{
    public function init()
    {
        $this
            ->setAttrib('id', 'form')
            ->setDecorators([
                'FormElements',
                ['HtmlTag', ['tag' => 'table']],
                'Form'
            ])
            ->addElement('hidden', 'act', [
                'value' => 'add_user',
            ])
            ->addElement('hidden', 'user_id', [
                'value' => '',
            ])
            ->addElement('note', 'basic_info', [
                'label' => 'Основная информация',
            ])
            ->addElement('text', 'name', [
                'label' => 'Имя *',
                'required' => true,
                'data-dojo-type' => 'dijit/form/ValidationTextBox',
                'data-dojo-props' => "regExp:'^[а-яА-ЯёЁa-zA-Z0-9]{1,15}'"
            ])
            ->addElement('text', 'surname', [
                'label' => 'Фамилия *',
                'required' => true,
                'data-dojo-type' => 'dijit/form/ValidationTextBox',
                'data-dojo-props' => "regExp:'^[а-яА-ЯёЁa-zA-Z0-9]{1,15}'"
            ])
            ->addElement('select', 'city', [
                'label' => 'Город',
                'multioptions' => [
                    0 => '- Выберите город -',
                    1 => 'Москва',
                    2 => 'Санкт-Петербург',
                    3 => 'Ижевск',
                ],
                'data-dojo-type' => 'dijit/form/Select',
            ])
            ->addElement('note', 'other_info', [
                'label' => 'Другая информация',
            ])
            ->addElement('textarea', 'comments', [
                'data-dojo-type' => 'dijit/form/Textarea',
            ])
            ->addElement('submit', 'submit', [
                'label' => 'Отправить',
                'data-dojo-type' => 'dijit/form/Button',
                'data-dojo-props' => "label:'Отправить'",
            ])
        ;

        $phone = new Form_Element_Phone('phone');
        $phone->setLabel('Телефон');
        $phone->setValidators([
            new Form_Validator_Phone(),
        ]);
        $this->addElement($phone);

        foreach ($this as $element) {
            $element->setDecorators([
                new Form_Decorator_Decorator(),
            ]);
        }


        $this
            ->addDisplayGroup(['basic_info', 'name', 'surname', 'city', 'phone'], 'first', [
                'decorators' => [
                    'FormElements',
                ]
            ])
            ->addDisplayGroup(['other_info', 'comments'], 'sec', [
                'decorators' => [
                    'FormElements',
                ]
            ])
        ;

    }

}