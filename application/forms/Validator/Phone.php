<?php

/**
 * Class Form_Validator_Phone
 */
class Form_Validator_Phone extends Zend_Validate_Abstract
{
    /**
     * @param mixed $value
     * @return bool
     */
    public function isValid($value)
    {
        if(!is_array($value)) return false;
        if(!isset($value['country_code']) || !isset($value['provide_code']) ||
            !isset($value['number']) || !isset($value['additional_number'])) return false;


        $country_code = $value['country_code'];
        $provide_code = $value['provide_code'];
        $number = $value['number'];
        $additional_number = $value['additional_number'];

        $len_validator = new Zend_Validate_StringLength();

        $len_validator->setMax(2);
        $len_validator->setMin(2);
        if(!$len_validator->isValid($country_code))
            $this->_messages[] = 'Код страны должен состоять из 2 цифр';

        $len_validator->setMax(4);
        $len_validator->setMin(4);
        if(!$len_validator->isValid($provide_code))
            $this->_messages[] = 'Код города должен состоять из 4 цифр';
        if(!$len_validator->isValid($additional_number))
            $this->_messages[] = 'Дополнительный номер должен состоять из 4 цифр';

        $len_validator->setMax(8);
        $len_validator->setMin(8);
        if(!$len_validator->isValid($number))
            $this->_messages[] = 'Номер телефона должен состоять из 8 цифр';

        $digits_validator = new Zend_Validate_Digits();

        if(!$digits_validator->isValid($country_code))
            $this->_messages[] = 'Код страны должен состоять только цифр';
        if(!$digits_validator->isValid($provide_code))
            $this->_messages[] = 'Код города должен состоять только цифр';
        if(!$digits_validator->isValid($number))
            $this->_messages[] = 'Номер телефона должен состоять только цифр';
        if(!$digits_validator->isValid($additional_number))
            $this->_messages[] = 'Дополнительный номер должен состоять только цифр';

        return (bool) !count($this->_messages);
    }
}