<?php

class Form_Decorator_Decorator extends Zend_Form_Decorator_Abstract
{

    public function render($content)
    {
        $element    = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return '';
        }

        $type = $element->getType();
        $label = htmlentities($element->getLabel());
        $id = $element->getId();
        $name = $element->getName();
        $value = $element->getValue();
        $is_required = $element->isRequired();

        $attrs = $element->getAttribs();
        $att = [];
        if(count($attrs)){
            foreach ($attrs as $k => $v) {
                if(!in_array($k, [
                    'data-dojo-type',
                    'data-dojo-props',
                ])) continue;
                $att[] = $k.'="'.$v.'"';
            }
        }
        $attributes = count($att)? implode(' ', $att): '';

        $result = '';
        switch ($type) {
            case 'Zend_Form_Element_Text':
                $format = '<tr><td><label for="%s">%s</label></td>'.
                    '<td><input type="text" id="%s" value="%s" name="%s" '.
                    ($is_required? 'required="required"': '').' '.
                    $attributes.
                    '></td></tr>';
                $result = sprintf($format, $id, $label, $id, $value, $name);
                break;
            case 'Zend_Form_Element_Note':
                $format = '<tr><td colspan="2">%s</td></tr>';
                $result = sprintf($format, $label);
                break;
            case 'Zend_Form_Element_Select':
                $format = '<tr><td><label for="%s">%s</label></td>'.
                    '<td><select id="%s" name="%s" '.$attributes.'>';
                $options = $element->getMultiOptions();
                if(count($options)) foreach ($options as $key => $value) {
                    $format .= '<option value="'.$key.'">'.$value.'</option>';
                }
                $format .= '</select> </td>';
                $result = sprintf($format, $id, $label, $id, $name);
                break;
            case 'Zend_Form_Element_Textarea':
                $format = '<tr><td colspan="2"><textarea name="%s" '.
                    ($is_required? 'required="required"': '').' '.
                    $attributes.
                    '>%s</textarea></td></tr>';
                $result = sprintf($format, $name, $value);
                break;
            case 'Zend_Form_Element_Submit':
                $format = '<tr><td colspan="2"><input type="submit" value="%s" '.
                $attributes.'></td></tr>';
                $result = sprintf($format, $label);
                break;
            case 'Form_Element_Phone':
                $format = '<tr><td><label>%s</label></td>'.
                    '<td> + <input type="text" name="phone_country_code" value="%s" 
                        data-dojo-type="dijit/form/ValidationTextBox"
                        data-dojo-props="regExp:\'\\d{2}\'"> '.
                    '(<input type="text" name="phone_provide_code" value="%s" 
                        data-dojo-type="dijit/form/ValidationTextBox"
                        data-dojo-props="regExp:\'\\d{4}\'">) '.
                    '<input type="text" name="phone_number" value="%s"
                        data-dojo-type="dijit/form/ValidationTextBox"
                        data-dojo-props="regExp:\'\\d{8}\'"> '.
                    '(доп. <input type="text" name="phone_additional_number" value="%s"
                        data-dojo-type="dijit/form/ValidationTextBox"
                        data-dojo-props="regExp:\'\\d{0,4}\'">)';
                $format .= '</td></tr>';
                $result = sprintf($format, $label, $value['country_code'],
                    $value['provide_code'], $value['number'], $value['additional_number']);
                break;
            case 'Zend_Form_Element_Hidden':
                $format = '<tr><td colspan="2"><input type="hidden" name="%s" value="%s"></td></tr>';
                $result = sprintf($format, $name, $value);
                break;
            default:
        }


        return $result;
    }
}