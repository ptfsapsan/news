<?php

/**
 * Class Form_Element_Phone
 */
class Form_Element_Phone extends Zend_Form_Element_Xhtml
{
    /**
     * @var string
     */
    protected $country_code = '';
    /**
     * @var string
     */
    protected $provide_code = '';
    /**
     * @var string
     */
    protected $number = '';
    /**
     * @var string
     */
    protected $additional_number = '';

    /**
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setCountryCode($value)
    {
        $this->country_code = (int) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getProvideCode()
    {
        return $this->provide_code;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setProvideCode($value)
    {
        $this->provide_code = (int) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setNumber($value)
    {
        $this->number = (int) $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdditionalNumber()
    {
        return $this->additional_number;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setAdditionalNumber($value)
    {
        $this->additional_number = (int) $value;
        return $this;
    }

    /**
     * @param mixed $value
     * @return $this
     * @throws Exception
     */
    public function setValue($value)
    {
        if(!is_array($value)) throw new \Exception('Неверное значение');
        if(!isset($value['country_code']) ||
            !isset($value['provide_code']) ||
            !isset($value['number']) ||
            !isset($value['additional_number']))
            throw new \Exception('Не все значения');

        $this->country_code = $value['country_code'];
        $this->provide_code = $value['provide_code'];
        $this->number = $value['number'];
        $this->additional_number = $value['additional_number'];

        return $this;
    }

    /**
     * @return array
     */
    public function getValue()
    {
        return [
            'country_code' => $this->country_code,
            'provide_code' => $this->provide_code,
            'number' => $this->number,
            'additional_number' => $this->additional_number,
        ];
    }


}