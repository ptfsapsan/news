<?php

class Model_Users extends Zend_Db_Table_Abstract
{
    public function getAll()
    {
        return $this->_db->fetchAll("SELECT * FROM users");
    }

    public function getById($id)
    {
        return $this->_db->fetchRow("SELECT * FROM users WHERE id = ?", $id);
    }

    public function addUser($params)
    {
        $this->_db->insert('users', [
            'name' => $params['name'],
            'surname' => $params['surname'],
            'city' => $params['city'],
            'phone_country_code' => $params['phone_country_code'],
            'phone_provide_code' => $params['phone_provide_code'],
            'phone_number' => $params['phone_number'],
            'phone_additional_number' => $params['phone_additional_number'],
        ]);
    }

    public function editUser($params)
    {
        $this->_db->update('users', [
            'name' => $params['name'],
            'surname' => $params['surname'],
            'city' => $params['city'],
            'phone_country_code' => $params['phone_country_code'],
            'phone_provide_code' => $params['phone_provide_code'],
            'phone_number' => $params['phone_number'],
            'phone_additional_number' => $params['phone_additional_number'],
        ], "id = ".(int)$params['user_id']);
    }
}