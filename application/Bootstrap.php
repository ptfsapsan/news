<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap{

   protected $_config;
   protected $_cache;

   protected function _initPluginLoaderCache(){
       if(APPLICATION_ENV == 'production'){
         $classFileIncCache
            = APPLICATION_PATH.'/data/cache/pluginLoaderCache.php';

         if(file_exists($classFileIncCache))
            include_once $classFileIncCache;

         Zend_Loader_PluginLoader::setIncludeFileCache($classFileIncCache);
      }
   }

   protected function _initAutoload(){
      $moduleLoader = new Zend_Application_Module_Autoloader(array(
         'namespace' => '',
         'basePath' => APPLICATION_PATH,
      ));

      return $moduleLoader;
   }

   protected function _initConfig(){
      $this->_config = new Zend_Config($this->getOptions());
      Zend_Registry::set('config', $this->_config);
      Zend_Registry::set('env', APPLICATION_ENV);
   }

   public function _initDb(){
      $db = Zend_Db::factory($this->_config->resources->db);
      Zend_Db_Table_Abstract::setDefaultAdapter($db);
   }

   protected function _initSession(){
      $ccp = session_get_cookie_params();
      $domain = $this->_config->domain;
      $lifetime = 43200;
      session_set_cookie_params($lifetime, $ccp['path'], $domain,
         $ccp['secure'], $ccp['httponly']);
      $config = array(
         'name' => 'sessions',
         'primary' => 'id',
         'modifiedColumn' => 'modified',
         'dataColumn' => 'data',
         'lifetimeColumn' => 'lifetime',
         'lifetime' => $lifetime,
      );
      Zend_Session::start();
   }


//   protected function _initLogger(){
//      if ($this->hasPluginResource('log')) {
//         $log = $this->getPluginResource('log')->getLog();
//         Zend_Registry::set('log', $log);
//      }
//   }


   protected function _initRoutes(){
      include_once APPLICATION_PATH.'/configs/router.php';
      $router = new Router;
      $router->getRouts();
      Zend_Controller_Front::getInstance()->setRouter($router);
   }


   protected function _initView(){
      $this->bootstrap('layout');
      $layout = $this->getResource('layout');
      $view = $layout->getView();

      $url = $view->serverUrl();
      $view->doctype('HTML5');
      $view->headTitle('')->setSeparator(' | ');
      $view->headMeta()->appendHttpEquiv('Content-Type',
         'text/html; charset=UTF-8')
         ->setName('keywords', '')
         ->setName('description', '')
         ->appendName('viewport',
            'width=device-width, initial-scale=1, maximum-scale=1');
      $view->headLink()->headLink(array('rel' => 'favicon',
         'href' => $url.'/favicon.ico',
         'type' => 'image/x-icon'))
         ->headLink(array('rel' => 'shortcut icon',
            'href' => $url.'/favicon.ico',
            'type' => 'image/x-icon'))
         ->headLink(array('rel' => 'stylesheet', 'type' => 'text/css',
            'href' => '//fonts.googleapis.com/css?family=Open+Sans:400,600,700&subset=latin,cyrillic'))
      ;
   }


}




