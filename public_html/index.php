<?php
ini_set('display_errors', true);
defined('DOCUMENT_ROOT') || define('DOCUMENT_ROOT', realpath(dirname(__FILE__)));
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(DOCUMENT_ROOT .
    '/../application'));
defined('LIB_PATH') || define('LIB_PATH', realpath(DOCUMENT_ROOT . '/../library'));

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ?
   getenv('APPLICATION_ENV') :
    //'production'));
    'development'));

set_include_path(implode(PATH_SEPARATOR, array(LIB_PATH, get_include_path(), APPLICATION_PATH.'/models')));

require_once 'Zend/Application.php';

$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);
$application->bootstrap()
            ->run();